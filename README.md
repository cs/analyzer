# Analyzer
An analysis framework designed to be used in conjunction with qcontrol3.

## Features
* analysis over single files
* parallelism by running on multiple files on the same time

### Future plans
* handle missing parameters in some files
* analyser over sequences
* add a daemon automatically processing newly incoming shots
* create a GUI
* test dask for parallelism and task structuring (?)
