from setuptools import setup, find_namespace_packages

with open("requirements.txt", "r") as f:
    requirements = f.read().splitlines()

setup(
    name="cs-analyzer",
    version="1.0.0",
    packages=find_namespace_packages(include=["cs.*"]),
    url="https://gitlab.physik.uni-muenchen.de/cs/analyzer",
    license="GPL-3",
    author="Hendrik v. Raven",
    author_email="hendrik@consetetur.de",
    description="Data analysis framework for the Caesium lab",
    install_requires=requirements,
    test_requires=["pytest", "qcontrol3_base"],
    test_suite="pytest",
)
