from cs.units import u
import datetime
import h5py
import numpy as np
import os
from pathlib import Path


base_path = os.environ.get("CS_MEASUREMENTS_DIR", "/mnt/data/measurements")


def _list_hdf_in_folder(day, subfolder, base_path):
    try:
        day = datetime.date.fromisoformat(day)
    except TypeError:
        pass
    dir = Path(base_path) / day.strftime("%Y/%m/%d") / subfolder
    if not dir.exists():
        raise FileNotFoundError(
            f"Directory {subfolder} of day {day} does not exist! ({dir})"
        )
    return dir.glob("*.hdf5")


class HDF5Loader:
    def __init__(self, base_path=base_path):
        self.base_path = base_path

    @staticmethod
    def load(file_path, data_paths):
        try:
            out = {}
            with h5py.File(file_path, "r") as f:
                for data_path in data_paths:
                    value = np.asarray(f[data_path])
                    if "unit" in f[data_path].attrs:
                        unit = f[data_path].attrs["unit"]
                        if isinstance(unit, str):
                            # new format
                            value *= u.Unit(unit.replace(".0", ""))
                        else:
                            # old format, list of exponents
                            value *= u.m ** unit[0]
                            value *= u.kg ** unit[1]
                            value *= u.s ** unit[2]
                            value *= u.A ** unit[3]
                            value *= u.K ** unit[4]
                            value *= u.cd ** unit[5]
                            value *= u.mol ** unit[6]

                    out[data_path] = value
            return out
        except OSError as e:
            raise OSError(str(e) + f" ({file_path})")
        except KeyError:
            raise KeyError(f"Key not found: {data_path} (file: {file_path}")

    def list_folder(self, day, subfolder):
        files = _list_hdf_in_folder(day, subfolder, self.base_path)
        return filter(lambda f: "expconfig" not in f.name, files)

    def list_iteration_files(self, day, iteration_name):
        files = sorted(
            list(
                map(
                    # replace _ with - as it is less than .
                    # this makes sure that the expconfig is at the start of each new
                    # run. Else it would appear behind the first element of each run
                    lambda s: str(s).replace("_expconfig", "-expconfig"),
                    _list_hdf_in_folder(day, f"it_{iteration_name}", self.base_path),
                )
            )
        )
        out = []
        curr = []
        for file in files:
            if "expconfig" in file:
                if curr: # if curr is no empty
                    out.append(curr)
                    curr = []
            else:
                curr.append(file)
        out.append(curr)
        if len(out) < 1:
            raise FileNotFoundError(
                f"could not find any iteration files for iteration {iteration_name} on day {day} (basepath={self.base_path})"
            )
        return out
