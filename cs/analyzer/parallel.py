import atexit
import multiprocess as multiprocessing


def is_interactive():
    import __main__ as main

    return not hasattr(main, "__file__")


def init_parallel():
    """load everything required to use the parallel feature.

    Currently it is used to import ipyparallel in notebooks so that %%px can
    be used once any component of the analyzer is imported
    """
    if is_interactive():
        import ipyparallel as ipp


class NullParallel:
    def map(self, fun, iterable):
        return map(fun, iterable)

    def cleanup(self):
        pass


class MultiprocessingParallel:
    def __init__(self):
        self._pool = multiprocessing.Pool()

    def __del__(self):
        self.cleanup()

    def cleanup(self):
        if self._pool:
            self._pool.terminate()
            self._pool.join()

    def map(self, fun, iterable):
        return self._pool.imap(fun, iterable)


class IPyParallel:
    def __init__(self):
        import ipyparallel as ipp

        self._c = ipp.Client()
        self._dview = self._c[:]
        self._dview.use_dill()

    def map(self, fun, iterable):
        return self._dview.map_sync(fun, iterable)

    def cleanup(self):
        pass


class Parallel:
    def __init__(self):
        raise Exception

    _impl = None

    @classmethod
    def get(cls, method=None):
        if cls._impl is None:
            if method is not None:
                cls._impl = method()
            elif is_interactive():
                # multiprocessing.Pool does not work well with interactive mode
                cls._impl = IPyParallel()
            else:
                cls._impl = MultiprocessingParallel()
            atexit.register(cls.cleanup)
        return cls._impl

    @classmethod
    def cleanup(cls):
        if cls._impl:
            cls._impl.cleanup()
            cls._impl = None
