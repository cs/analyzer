from dataclasses import dataclass
import datetime
import numpy as np
from functools import reduce
import pandas as pd
import pathlib
from typing import Any, Dict, List, Sequence, Tuple
import xarray as xr

from cs.analyzer.error import AnalyzerError


@dataclass
class TempVariable:
    source: str
    dims: Sequence[str]
    suffix: str
    data: Any

    def to_xarray(self):
        if isinstance(self.data, xr.DataArray):
            return self.data

        d, attrs = _split_unit(self.data)
        return xr.DataArray(data=np.asanyarray(d), attrs=attrs)


iter_paths = [
    "task/iter/enabled",
    "task/iter/step",
    "task/iter/value",
    "task/run_timestamp",
]


def _expand_value_dicts(d):
    """expand a returned dictionary in a list of entries

    :param d: Dict
        returned entry from one analyzer for construction of a xr.Dataset
    :return: List[Dict]
    """
    orig_name, v = d
    idx, l_analyzer = v
    if isinstance(l_analyzer[0], dict):
        return [
            (f"{orig_name}_{name}", (idx, [value]))
            for name, value in l_analyzer[0].items()
        ]
    else:
        return [d]


def _split_unit(values: Any) -> Tuple[List[Any], Dict[str, str]]:
    """separate units into attrs dictionary
    xarray does not support units. Instead we split it off into the attrs
    dictionary which can be used for extra metadata
    """
    try:
        temp = values.value, {"unit": str(values.unit)}
        return temp
    except AttributeError:
        return values, {}


def _check_dimensions(data: Any, dims: Sequence[str]):
    """verify that the output data matches the specified dimensions
    """
    d = np.asanyarray(data)
    if len(d.shape) != len(dims):
        raise AnalyzerError(
            f"output dimensions do not match. Expected {dims}, "
            f"but received data with shape {d.shape} (data: {d})"
        )


def _run_analyzers(analyzers, file_data, filename, output_mapping, dimension_names):
    if dimension_names == "time":
        idx_value = pd.Timestamp(
            datetime.datetime.fromisoformat(
                file_data["task/run_timestamp"][()].decode()
            )
        )
        coords = {"time": [idx_value]}
    else:
        # either just "iter" or specified dimension names
        idx_value = file_data["task/iter/value"]

        if dimension_names != "iter":
            indices = dimension_names
            coords = {idx: value for idx, value in zip(indices, idx_value)}
        elif len(idx_value.shape) > 0:
            indices = (
                "iter",
                *(f"iter{n}" for n in range(2, idx_value.shape[0] + 2)),
            )
            coords = {idx: value for idx, value in zip(indices, idx_value)}
        else:
            coords = {"iter": [idx_value]}

    sequence_data = {}

    for analyzer in analyzers:
        try:
            parameters_data = {
                param_name: sequence_data[dep_name].data
                for param_name, dep_name in analyzer.parameter_mapping.items()
            }
            value = analyzer.instance.get(file_data, parameters_data)

            if isinstance(value, dict):
                # if returned value is a dict we expand it into multiple entries
                entries = {f"_{k}": v for k, v in value.items()}

            else:
                entries = {"": value}

            data_dict = {}
            for suffix, data in entries.items():
                _check_dimensions(data, analyzer.instance.dims)

                var = TempVariable(
                    source=analyzer.name,
                    dims=analyzer.instance.dims,
                    suffix=suffix,
                    data=data,
                )

                name = analyzer.name + suffix
                assert name not in sequence_data
                sequence_data[name] = var

                if suffix:
                    data_dict[suffix[1:]] = data

            if data_dict:
                # add all data as dict into the sequence data to be used by
                # following scripts
                sequence_data[analyzer.name] = TempVariable(
                    source=analyzer.name, dims=(), suffix="", data=data_dict
                )

        except AnalyzerError as e:
            raise AnalyzerError(
                f"failed to run {analyzer.instance} on {filename}"
            ) from e

    values = {
        output_mapping[var.source] + var.suffix: var.to_xarray()
        for name, var in sequence_data.items()
        if var.source in output_mapping
        and output_mapping[var.source] + var.suffix not in coords
    }
    values["file"] = xr.DataArray(pathlib.Path(filename).name)
    values["existing"] = xr.DataArray(True)

    return xr.Dataset(data_vars=values, coords=coords)


class Executor:
    def __init__(self, loader):
        self._loader = loader

    def run_on_file(self, filename, analyzer):
        data_paths = analyzer.get_dependencies()
        file_data = self._loader.load(filename, data_paths)
        return analyzer.get(file_data)

    def run_multiple_on_file(
        self,
        filename,
        analyzers,
        data_paths,
        output_mapping,
        dimension_names,
        extra_data_paths=iter_paths,
    ):
        data_paths += extra_data_paths
        file_data = self._loader.load(filename, data_paths)
        return _run_analyzers(
            analyzers, file_data, filename, output_mapping, dimension_names
        )

    def run_series_analyzers(self, analyzers, data):
        raise NotImplementedError
        for analyzer in analyzers:
            values = xr.DataArray(
                [analyzer.get(data.sel(run=run)) for run in data.run.values], dims="run"
            )
            data[analyzer.name] = values
        return data
