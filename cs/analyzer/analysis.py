from dataclasses import dataclass, field
from datetime import datetime
from typing import Any, Callable, Dict, List, Sequence
import cs.units as u
import inspect
import json
import re


from cs.analyzer.parallel import init_parallel

init_parallel()


def _parse_quantity(value: str) -> u.Quantity:
    """parse an input as astropy quantity

    this function tries to convert qunits string representation into astropy
    string representation where possible.

    qunits expresses every unit in si base units with the format
    unit^power. astropy parses unit(power).

    :param value: str
    :return: u.Quantity
    """
    quant = u.Quantity(re.sub(r"(m|s|kg|mol|cd|K|A|m)\^([-.0-9]+)", r"\1(\2)", value))

    # return the original value if it would be dimensionless. Otherwise we parse
    # everything looking which could be parsed as a number as a quantity
    if quant.unit == u.dimensionless_unscaled:
        return value

    return quant


@dataclass
class Dependencies:
    """
    container class to contain all dependency informations
    """

    hdf_entries: List = field(default_factory=list)
    series: Dict = field(default_factory=dict)

    def __add__(self, other):
        return Dependencies(
            hdf_entries=self.hdf_entries + other.hdf_entries,
            series={**self.series, **other.series},
        )

    def __iadd__(self, other):
        self.hdf_entries += other.hdf_entries
        self.series.update(other.series)


class AnalysisBase:
    parameters = {}
    dims = ()

    def __init__(self, name=None, dims=None):
        if name:
            self._name = name
        if dims:
            self.dims = dims

    def get_dependencies(self):
        """list all dependencies required from files to run this classes
        analysis

        :return: Dependencies
            the required dependencies
        """
        return Dependencies()

    def get(self, file_data, parameters_data):
        raise NotImplementedError

    @property
    def name(self):
        try:
            return self._name
        except AttributeError:
            return self.__class__.__name__

    @property
    def node_name(self):
        """an internal name uniquely identifying a single instance

        this is very broad. Ideally subclasses implement a more specific
        version mapping different instances of the same object to one node_name
        """
        return str(self)


@dataclass
class Constant(AnalysisBase):
    value: Any

    def __init__(self, value, **kwargs):
        super().__init__(**kwargs)
        self.value = value

    def get(self, file_data, parameters_data):
        return self.value

    @property
    def node_name(self):
        return f"value_{self.value}"


Const = Constant


@dataclass
class HDF(AnalysisBase):
    path: str

    def __init__(self, path, **kwargs):
        super().__init__(**kwargs)
        self.path = path

    def get_dependencies(self):
        return Dependencies(hdf_entries=[self.path])

    def get(self, file_data, parameters_data):
        return file_data[self.path]

    @property
    def node_name(self):
        return f"hdf_{self.path}"


@dataclass
class Param(HDF):
    def __init__(self, path, *, parameter_path="task/parameters", **kwargs):
        path = parameter_path + "/" + path.replace(".", "/")
        super().__init__(path, **kwargs)


@dataclass
class Series(AnalysisBase):
    dep: object
    dim: str = "iter"

    def get_dependencies(self):
        return Dependencies(series={self.dim: self.dep.node_name})

    def get(self, series_data, parameters_data):
        return series_data[self.dep.node_name]


def _analyze_signature_parameters(function: Callable) -> Dict[str, Any]:
    """
    This allows to specify the required parameters as default arguments of
    the analyze function.
    """
    analyze_signature = inspect.signature(function)
    return {
        name: param.default
        for name, param in analyze_signature.parameters.items()
        if param.default is not inspect._empty
    }


class Analysis(AnalysisBase):
    def __init__(self, name=None, parameters={}, dims=None, **kwargs):
        super().__init__(name=name, dims=dims)

        self.parameters = self.__class__.parameters.copy()
        self.parameters.update(_analyze_signature_parameters(self.analyze))
        self.parameters.update(kwargs)
        self.parameters.update(parameters)

        self._check_parameters()

    @classmethod
    def from_function(cls, fun: Callable, dims: Sequence[str] = None, name: str = None):
        """construct an Analysis from a single function

        this function automatically constructs an Analysis class from a
        single function. Instead of manually specifying the paremetrs
        dictionary the functions default arguments are checked and
        converted into a matching parameters dict.

        Parameters
        ----------
        fun:
            function to convert into an analyzer
        """
        name = name if name else fun.__name__
        obj = cls(name=name, dims=dims)
        obj.analyze = fun
        obj.parameters.update(_analyze_signature_parameters(fun))
        return obj

    def __call__(self, *args, **kwargs):
        return self.analyze(*args, **kwargs)

    def _check_parameters(self):
        missing = [
            name
            for name in inspect.signature(self.analyze).parameters
            if name not in self.parameters
        ]
        if len(missing) > 0:
            raise AttributeError(
                f'Parameters "{missing}" are required but not defined for {self.name}'
            )

    def get_dependencies(self):
        return Dependencies()

    def get(self, file_data, parameters_data):
        return self.analyze(**parameters_data)

    # implement this function in your custom Analyser.
    # It gets all the arguments specified in the parameters dict.
    def analyze(self):
        raise NotImplementedError


def make_analysis(*args, name: str = None, dims: Sequence[str] = None):
    # args should only be populated when the decorator was used without
    # parantheses. In this case there is no need for the extra wrapper
    # and we are already given the function to analyze
    if len(args) == 1:
        return Analysis.from_function(*args)

    def wrapper(f):
        return Analysis.from_function(f, name=name, dims=dims)

    return wrapper


class Iteration(Analysis):
    parameters = dict(
        enabled=HDF("task/iter/enabled"),
        value=HDF("task/iter/value"),
        step=HDF("task/iter/step"),
    )

    def analyze(self, enabled, value, step):
        return dict(enabled=enabled, value=value, step=step)


class RunTimestamp(Analysis):
    parameters = dict(timestamp=HDF("task/run_timestamp"))

    def analyze(self, timestamp):
        return datetime.fromisoformat(timestamp[()].decode())


class ImageAnalysis(Analysis):
    dims = ["x", "y"]


class SeriesAnalysis(Analysis):
    pass


class SeriesFitAnalysis(SeriesAnalysis):
    parameters = dict()

    series_parameters = dict()

    fit_model = None

    def analyze(self, **params):
        raise NotImplementedError
