from cs.analyzer import Analysis, ImageAnalysis
from cs.units import u
import cs.constants as c

import numpy as np
from scipy.ndimage import maximum_filter, median_filter
import xarray as xr


class ScatteringCrossSection(Analysis):
    def analyze(self, intensity, detuning=0 * u.MHz, transition="f4f5_circ"):
        t = getattr(c.cs.d2, transition)
        return (
            t.sigma0
            / (1 + intensity / t.I_sat + 4 * detuning ** 2 / c.cs.d2.gamma ** 2)
        ).si


class AbsorptionImage(ImageAnalysis):
    @staticmethod
    def drop_dark_pixels(image, bright, dark, threshold=99):
        """convert not illuminated pixels to NaN values

        it filters pixels which are dark in the bright image and expands the
        area around those a bit
        """
        dark_threshold = np.percentile(dark, threshold)
        outer = maximum_filter(bright < dark_threshold, size=50)
        image[outer] = np.NaN
        return image

    def analyze(self, absorption, bright, dark, cross_section, pixel_size, threshold=99):
        absorption_sub = absorption - dark
        # detect unsigned integer overflows, 1 as we divide through it
        absorption_sub[absorption_sub > absorption] = 1
        absorption_sub[absorption_sub == 0] = 1
        bright_sub = bright - dark
        bright_sub[bright_sub > bright] = 1
        bright_sub[bright_sub == 0] = 1

        try:
            x_pixel_size, y_pixel_size = pixel_size
        except TypeError:
            x_pixel_size = y_pixel_size = pixel_size

        image = np.log(bright_sub / absorption_sub) / cross_section
        image = self.drop_dark_pixels(image, bright, dark, threshold)
        # move horizontal axis to first coordinate
        image = image.T

        return xr.DataArray(
            image,
            coords=(
                ("x", np.arange(image.shape[0]) * x_pixel_size),
                ("y", np.arange(image.shape[1]) * y_pixel_size),
            ),
        )


class CropImage(ImageAnalysis):
    def analyze(self, img, start_x, stop_x, start_y, stop_y):
        return img[start_x:stop_x, start_y:stop_y]
