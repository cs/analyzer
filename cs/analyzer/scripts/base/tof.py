import cs.constants as c
from cs.units import u
import numpy as np
import lmfit


def temperature(data, x=None, return_fit=False):
    def free_expansion(x, a, c):
        return np.sqrt(a * x ** 2 + c ** 2)

    if x is None:
        x = data["iter"]

    model = lmfit.Model(free_expansion)
    model.set_param_hint("a", min=0)
    model.set_param_hint("c", min=0)
    params = model.make_params(a=0.01, c=0.005)
    fitted = model.fit(data, x=x, params=params)
    T = (fitted.params["a"] * u.m ** 2 / u.s ** 2 * c.cs.m / c.k_B).to(u.K)

    if return_fit:
        return T, fitted
    else:
        return T
