from cs.analyzer import Analysis

import lmfit
import numpy as np
from scipy.ndimage import median_filter
from mpmath import fp
polylog = np.vectorize(fp.polylog)

def gauss_1d_guess(model, values, x, *, max_sigma=100):
    """try to guess good starting values for a 1d gauss
    
    :param model: Should always be the 1d Gaussian model with offset
    :param values: data values to estimate initial guess from
    :param x: the axis for the data values
    :param max_sigma: the maximum value of the initial guess for the cloud size
    
    :return: lmfit.Parameters to be used with model.fit
    """
    smoothed = median_filter(values, 5) # smooth data to not actual max and not some random spike

    i_max = np.argmax(smoothed)

    values = values

    amplitude = values[i_max] # amplitude should be the previously found maximum
    delta_x = x[1] - x[0]     # x-sampling, to be used for size estimate
    sigma = (
        np.min(
            (
                max_sigma,
                np.argmin(np.abs(amplitude / 2 - values[i_max:])) # first index>i_max where the values are smaller than the half amplitude
                / np.sqrt(2 * np.log(2)) * delta_x,
            ) #size is either max_sigma or the sigma computed from HWHM
        )
    )
    amplitude *= np.sqrt(2 * np.pi) * sigma # scale amplitude properly for normal distr. definition
    offset = values.min()#[int(i_max + 3 * sigma) % len(values)]

    return model.make_params(
        amplitude=amplitude, sigma=sigma, center=x[i_max], c=offset
    )


def make_gauss_1d_model():
    """
    :return: lmfit.Model made out of Gaussian and Constant model.
    
    model parameters have limits:
      0 <= amplitude
      0 <= sigma <= 200
    """
    gauss = lmfit.models.GaussianModel()
    offset = lmfit.models.ConstantModel()
    model = gauss + offset
    model.set_param_hint("amplitude", min=0)
    model.set_param_hint("sigma", min=0, max=200)
    model.set_param_hint("c") #removed positive offset constraint
    return model


def gauss_1d(values, x=None, guess=None, **kwargs):
    """perform a 1d gauss fit with constant offset on the values
    
    :param values: the data values
    :param x: the axis parameterizing the values. if None, is created using arange
    :param guess: the initial guess for the fit, if None guess is found using gauss_1d_guess
    
    :return: lmfit.ModelResult of the fit to the data
    """
    model = make_gauss_1d_model()

    if x is None:
        x = np.arange(len(values))

    if guess:
        params = model.make_params(**guess)
    else:
        params = gauss_1d_guess(model, values, x)

    fit = model.fit(data=np.nan_to_num(values), params=params, x=x, **kwargs)

    return fit


def bimodal_gauss_1d(values, x=None, guess=None, sigma_cut=5e-4, method="least_squares", **kwargs):
    """fit the values using a bimodal gaussian distribution

    :param values: 1d array
    :param x: 1d array (optional)
        indices to use for the data. If not present indices starting from 0 are used
    :param guess: lmfit.Parameters
        initial guess for the bimodal fit
    :param sigma_cut: float
        maximum sigma value for the smaller gaussian distribution and minimum sigma value
        for the larger distribution
    :param method: str
        fitting method to use. See lmfit documentation for more details
    :param kwargs:
        extra parameters passed to the fit function
    :return: lmfit.ModelResult
        fit result
    """
    bimodal_model = lmfit.models.GaussianModel(prefix='g1_') + lmfit.models.GaussianModel(
        prefix='g2_') + lmfit.models.ConstantModel()
    bimodal_model.set_param_hint('g1_amplitude', min=0)
    bimodal_model.set_param_hint('g2_amplitude', min=0)
    bimodal_model.set_param_hint('g1_sigma', min=0, max=sigma_cut)
    bimodal_model.set_param_hint('g2_sigma', min=sigma_cut)

    if x is None:
        x = np.arange(values)

    if guess:
        params = bimodal_model.make_params(**guess)
    else:
        gauss_model = make_gauss_1d_model()
        init_params = gauss_1d_guess(gauss_model, values, x, max_sigma=sigma_cut)

        params = bimodal_model.make_params(
            g1_center=init_params["center"],
            g2_center=init_params["center"],
            g1_amplitude=init_params["amplitude"]/2,
            g2_amplitude=init_params["amplitude"]/2,
            g1_sigma=init_params["sigma"],
            g2_sigma=init_params["sigma"]*2,
            c=init_params["c"]
        )

    return bimodal_model.fit(data=np.nan_to_num(values), params=params, x=x, method=method, **kwargs)


def gauss_1d_xy(data):
    """ fit 1d gaussian to 1st and 2nd dim of 2d nd-array and return lmfit.ModelResult for both

    :return:
        xfit: lmfit.ModelResult for 1st dimension of data
        yfit: lmfit.ModelResult for 2nd dimension of data"""
    xdata = data.sum(1)
    ydata = data.sum(0)
    fitx = gauss_1d(xdata)
    fity = gauss_1d(ydata)

    return fitx, fity


def gauss_2d_function(x, y, x0, y0, sx, sy, A):
    """ 2d normal distribution. 
    
    x and y should be 2d arrays, e.g. generated with np.meshgrid()
    
    function is normalized to one, i.e. 
        gauss_2d(0) = A/2/pi/sx/sy
        
    gauss_2d does not include an offset
    """
    return (
        A
        / 2
        / np.pi
        / sx
        / sy
        * np.exp(-((x - x0) ** 2) / 2 / sx ** 2)
        * np.exp(-((y - y0) ** 2) / 2 / sy ** 2)
    )


gauss_2d_model = lmfit.Model(gauss_2d_function, independent_vars=["x", "y"])


def gauss_2d_guess(data, x=None, y=None, filter_size=5, guesssizemax=20):
    """ create initial guess for gauss_2d_model """
    if x is None:
        if y is None:
            x, y = np.indices(data)
        x, _ = np.indices(data)
    if y is None:
        _, y = np.indices(data)

    data = median_filter(data, filter_size)
    x0, y0 = np.unravel_index(np.argmax(data), data.shape)
    A = data[x0, y0]
    sx = max(
        guesssizemax, np.abs(np.argmin(np.abs(A / 2 - data[x0:, y0])) / np.sqrt(2 * np.log(2)))
    )
    sy = max(
        guesssizemax, np.abs(np.argmin(np.abs(A / 2 - data[x0, y0:])) / np.sqrt(2 * np.log(2)))
    )
    A = A * 2 * np.pi * sx * sy  # correct norm for A

    x0, y0 = x[y0,x0], y[y0,x0]
    sx = sx*(x[0,1]-x[0,0]) 
    sy = sy*(y[1,0]-y[0,0])
    
    gauss_2d_model.set_param_hint("x0", value=x0, min=x.min(), max=x.max())
    gauss_2d_model.set_param_hint("y0", value=y0, min=y.min(), max=y.max())
    gauss_2d_model.set_param_hint("sx", value=sx, min=0, max=x.max() - x.min())
    gauss_2d_model.set_param_hint("sy", value=sy, min=0, max=y.max() - y.min())
    gauss_2d_model.set_param_hint("A", value=A, min=0, max=10 * A)

    return gauss_2d_model.make_params()


gauss_2d_model.guess = gauss_2d_guess


def gauss_2d(data, x=None, y=None, filter_size=5, guesssizemax=20):
    """ fit 2d gauss to data 
    
    Returns:
        f: lmfit.ModelResult for 2d gauss fit"""
    if x is None:
        if y is None:
            x, y = np.indices(data)
        x, _ = np.indices(data)
    if y is None:
        _, y = np.indices(data)

    g2d = gauss_2d_model
    o = lmfit.models.ConstantModel()

    init = o.make_params(x=data.min())
    init += g2d.guess(data - data.min(), x, y, filter_size, guesssizemax)
    f = (o + g2d).fit(data, init, x=x, y=y)
    return f



def polylog_52_function(x, x0, A, size, mu):
    return A * polylog(5/2, np.exp(-2*(x-x0)**2/size**2 + mu))
    
polylog_52_model = lmfit.Model(polylog_52_function)
polylog_52_model.set_param_hint('mu', max=0)
polylog_52_model.set_param_hint('A', min=0)
polylog_52_model.set_param_hint('size', min=0)

def polylog_52_guess(data, x=None, muinit=0):
    if x is None:
        x = np.arange(data.size)
    
    p = gauss_1d_guess(make_gauss_1d_model(), values=data, x=x)
    
    return polylog_52_model.make_params(
        x0=p['center'], size=p['sigma'], A=p['amplitude']/np.sqrt(2*np.pi*p['sigma']**2), mu=muinit
    )

polylog_52_model.guess = polylog_52_guess

def fit_polylog_52(data, x=None):
    if x is None:
        x = np.arange(data.size)
    
    p52 = polylog_52_model
    o = lmfit.models.ConstantModel()
    
    init = o.make_params(x=data.min())
    init += p52.guess(data - data.min(), x)
    f = (o + p52).fit(data, init, x=x)
    
    return f