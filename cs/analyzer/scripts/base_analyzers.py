import cs.analyzer.scripts.mot as mot_base
from cs.analyzer.scripts.base.fit import bimodal_gauss_1d, make_gauss_1d_model
from cs.analyzer.analyzer import Analyzer
import numpy as np
import cs.units as u
from cs.analyzer.analysis import Analysis

def image_analyzer(img_path, absimg='1', brightimg='2', darkimg='3', crop=None):
    absorption = mot_base.HDF(f'/data/{img_path}/image/{absimg}/data', name='absorption', dims=('y', 'x'))
    bright = mot_base.HDF(f'/data/{img_path}/image/{brightimg}/data', name='bright', dims=('y', 'x'))
    dark = mot_base.HDF(f'/data/{img_path}/image/{darkimg}/data', name='dark', dims=('y', 'x'))

    cross_section = mot_base.ScatteringCrossSection(
        name='cross_section',
        intensity=mot_base.Param(f'measured.imaging.{img_path}.intensity'),
        transition=mot_base.MOTImagingTransition(),
    )
    pixel_size = mot_base.Param(f'measured.imaging.{img_path}.pixel_size', name='pixel_size')

    OD = mot_base.AbsorptionImage(
        name='OD',
        absorption=absorption,
        dark=dark,
        bright=bright,
        pixel_size=pixel_size,
        cross_section=cross_section,
    )

    if crop is None:
        return Analyzer(absorption, bright, dark, cross_section, pixel_size, OD)
    else:
        cropOD = mot_base.CropImage(img=OD,
                           start_x=crop[0],
                           start_y=crop[1],
                           stop_x=crop[2],
                           stop_y=crop[3],
                           name='cropOD',
                           dims=('v', 'u'))

    return Analyzer(absorption, bright, dark, cross_section, pixel_size, OD, cropOD)

class T_est(mot_base.Analysis):
    def analyze(self, delay, sigma):
        return sigma**2*pc.cs.m/pc.k_B/delay**2

def gauss_analyzer(OD):

    gauss_x = mot_base.ImageGaussFit(name='gauss_x', image=OD, axis='x')
    gauss_y = mot_base.ImageGaussFit(name='gauss_y', image=OD, axis='y')
    atom_number = mot_base.AtomNumber(name='atom_number', x_fit=gauss_x, y_fit=gauss_y)
    density = mot_base.Density(name='density', atom_number=atom_number, x_fit=gauss_x, y_fit=gauss_y)
    sigma_x = mot_base.FitParameter(name='sigma_x', fit=gauss_x, parameter='sigma')
    sigma_y = mot_base.FitParameter(name='sigma_y', fit=gauss_y, parameter='sigma')
    sigma_x = mot_base.FitParameter(name='sigma_x', fit=gauss_x, parameter='sigma')
    sigma_y = mot_base.FitParameter(name='sigma_y', fit=gauss_y, parameter='sigma')

    imaging_delay = mot_base.Param('sequence.imaging.delay', name='imaging_delay')
    temperature_estimate_x = T_est(name='T_x', delay=imaging_delay, sigma=sigma_x)
    temperature_estimate_y = T_est(name='T_y', delay=imaging_delay, sigma=sigma_y)

    return Analyzer(gauss_x, gauss_y, atom_number, density, sigma_x,
                    sigma_y, imaging_delay, temperature_estimate_x, temperature_estimate_y)

def bm_gauss_analyzer(OD, sigma_cut=200):

    class AtomNumber(Analysis):
        """ extract atom number from double gauss fit"""

        def analyze(self, x_fit, y_fit):
            return (x_fit.params["g1_amplitude"] + y_fit.params["g1_amplitude"]) / 2

    class Density(Analysis):
        """ compute density from atom number and cloud sizes"""

        def analyze(self, atom_number, sigma_x, sigma_y):
            return (
                    atom_number
                    / np.sqrt(sigma_x ** 3 * sigma_y ** 3)
                    / np.sqrt(8 * np.pi ** 3)
                    / u.m ** 3
            ).to(1 / u.cm ** 3)

    def bimodal_gauss_1d_e(values, **kwargs):
        """ change sigma_cut, because the default is too small"""
        return bimodal_gauss_1d(values, sigma_cut=sigma_cut, **kwargs)

    bm_gauss_x = mot_base.ImageGaussFit(name="bm_gauss_x", image=OD, axis="x", function=bimodal_gauss_1d_e)
    bm_gauss_y = mot_base.ImageGaussFit(name="bm_gauss_y", image=OD, axis="y", function=bimodal_gauss_1d_e)

    N = AtomNumber(name='N', x_fit=bm_gauss_x, y_fit=bm_gauss_y)
    sx = mot_base.FitParameter(name="sx", fit=bm_gauss_x, parameter="g1_sigma")
    sy = mot_base.FitParameter(name="sy", fit=bm_gauss_y, parameter="g1_sigma")
    sx2 = mot_base.FitParameter(name="sx2", fit=bm_gauss_x, parameter="g2_sigma")
    sy2 = mot_base.FitParameter(name="sy2", fit=bm_gauss_y, parameter="g2_sigma")
    n = Density(name='density', atom_number=N, sigma_x=sx, sigma_y=sy)

    imaging_delay = mot_base.Param('sequence.imaging.delay', name='imaging_delay')
    temperature_estimate_x = T_est(name='T_x', delay=imaging_delay, sigma=sx)
    temperature_estimate_y = T_est(name='T_y', delay=imaging_delay, sigma=sy)

    return Analyzer(bm_gauss_x, bm_gauss_y, sx, sy, sx2, sy2, n,
                    imaging_delay, temperature_estimate_x, temperature_estimate_y)
