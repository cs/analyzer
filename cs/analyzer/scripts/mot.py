from cs.analyzer.scripts.base.imaging import AbsorptionImage, ScatteringCrossSection
from cs.analyzer.scripts.base.fit import gauss_1d, bimodal_gauss_1d
from cs.analyzer.analysis import Analysis, HDF, Param
from cs.units import u
import cs.constants as pc
import numpy as np


class MOTImagingTransition(Analysis):
    parameters = dict(
        transition=Param("sequence.imaging.transition"), polarisation="isopol"
    )

    def analyze(self, transition, polarisation):
        if transition == "32" or transition == b"32":
            return f"f3f2_{polarisation}"
        elif transition == "45" or transition == b"45":
            return f"f4f5_{polarisation}"
        else:
            raise Exception(f"Unknown transition {transition}")


class ImageGaussFit(Analysis):
    def __init__(self, *, function=gauss_1d, **kwargs):
        super().__init__(**kwargs)
        self._function = function

    def analyze(self, image, axis):
        if axis == "x":
            contra = "y"
        else:
            contra = "x"

        dx = image[contra].values[1] - image[contra].values[0]

        return self._function(image.sum(contra).values * dx, x=image[axis])


class FitParameter(Analysis):
    def analyze(self, fit, parameter):
        return fit.params[parameter]


class AtomNumber(Analysis):
    def analyze(self, x_fit, y_fit):
        return (x_fit.params["amplitude"] + y_fit.params["amplitude"]) / 2


class Density(Analysis):
    def analyze(self, atom_number, x_fit, y_fit):
        sigma_x = x_fit.params["sigma"]
        sigma_y = y_fit.params["sigma"]
        return (
            atom_number
            / np.sqrt(sigma_x ** 3 * sigma_y ** 3)
            / np.sqrt(8 * np.pi ** 3)
            / u.m ** 3
        ).to(1 / u.cm ** 3)


class PSD(Analysis):
    def analyze(self, density, temperature):
        return density * pc.h ** 3 / (pc.cs.m * pc.k_B * temperature) ** (3 / 2)


x_absorption = HDF(
    "/data/mot_mako/image/1/data", name="mot_mako_absorption", dims=("y", "x")
)
x_dark = HDF("/data/mot_mako/image/3/data", name="mot_mako_dark", dims=("y", "x"))
x_bright = HDF("/data/mot_mako/image/2/data", name="mot_mako_bright", dims=("y", "x"))
x_cross_section = (
    ScatteringCrossSection(
        name="mot_mako_cross_section",
        intensity=Param("measured.imaging.mot_mako.intensity"),
        transition=MOTImagingTransition(),
    ),
)

x_image = AbsorptionImage(
    name="mot_mako",
    absorption=x_absorption,
    dark=x_dark,
    bright=x_bright,
    pixel_size=Param("measured.imaging.mot_mako.pixel_size"),
    cross_section=x_cross_section,
)
x_gauss_x = ImageGaussFit(name="mot_mako_gauss_x", image=x_image, axis="x")
x_gauss_y = ImageGaussFit(name="mot_mako_gauss_y", image=x_image, axis="y")
x_bm_gauss_x = ImageGaussFit(name="mot_mako_gauss_x", image=x_image, axis="x", function=bimodal_gauss_1d)
x_bm_gauss_y = ImageGaussFit(name="mot_mako_gauss_y", image=x_image, axis="y", function=bimodal_gauss_1d)
x_atom_number = AtomNumber(
    name="mot_mako_atom_number", x_fit=x_gauss_x, y_fit=x_gauss_y
)
x_density = Density(
    name="mot_mako_density", atom_number=x_atom_number, x_fit=x_gauss_x, y_fit=x_gauss_y
)


class X:
    absorption = (x_absorption,)
    dark = x_dark
    bright = x_bright
    image = x_image
    gauss_x = x_gauss_x
    gauss_y = x_gauss_y
    sigma_x = FitParameter(name="mot_mako_sigma_x", fit=x_gauss_x, parameter="sigma")
    sigma_y = FitParameter(name="mot_mako_sigma_y", fit=x_gauss_y, parameter="sigma")
    bm_gauss_x = x_bm_gauss_x
    bm_gauss_y = x_bm_gauss_y
    bm_sigma_x = FitParameter(name="mot_mako_bm_sigma_x", fit=x_bm_gauss_x, parameter="g1_sigma")
    bm_sigma_y = FitParameter(name="mot_mako_bm_sigma_y", fit=x_bm_gauss_y, parameter="g1_sigma")
    bm_sigma2_x = FitParameter(name="mot_mako_bm_sigma2_x", fit=x_bm_gauss_x, parameter="g2_sigma")
    bm_sigma2_y = FitParameter(name="mot_mako_bm_sigma2_y", fit=x_bm_gauss_y, parameter="g2_sigma")
    atom_number = x_atom_number
    density = x_density
    imaging_delay = Param("sequence.imaging.delay", name="imaging_delay")
    cross_section = x_cross_section


y_absorption = HDF(
    "/data/mot/image/1/data", name="mot_manta_absorption", dims=("y", "x")
)
y_dark = HDF("/data/mot/image/3/data", name="mot_manta_dark", dims=("y", "x"))
y_bright = HDF("/data/mot/image/2/data", name="mot_manta_bright", dims=("y", "x"))
y_cross_section = ScatteringCrossSection(
    name="mot_manta_cross_section",
    intensity=Param("measured.imaging.mot.intensity"),
    transition=MOTImagingTransition(),
)

y_image = AbsorptionImage(
    name="mot_manta",
    absorption=y_absorption,
    dark=y_dark,
    bright=y_bright,
    pixel_size=Param("measured.imaging.mot.pixel_size"),
    cross_section=y_cross_section,
)
y_gauss_y = ImageGaussFit(name="mot_manta_gauss_x", image=y_image, axis="y",)
y_gauss_x = ImageGaussFit(name="mot_manta_gauss_y", image=y_image, axis="x")
y_bm_gauss_x = ImageGaussFit(name="mot_manta_bm_gauss_x", image=y_image, axis="x", function=bimodal_gauss_1d)
y_bm_gauss_y = ImageGaussFit(name="mot_manta_bm_gauss_y", image=y_image, axis="y", function=bimodal_gauss_1d)
y_atom_number = AtomNumber(
    name="mot_manta_atom_number", y_fit=y_gauss_y, x_fit=y_gauss_x
)
y_density = Density(
    name="mot_manta_density",
    atom_number=y_atom_number,
    y_fit=y_gauss_y,
    x_fit=y_gauss_x,
)


class Y:
    absorption = y_absorption
    dark = y_dark
    bright = y_bright
    image = y_image
    gauss_x = y_gauss_x
    gauss_y = y_gauss_y
    sigma_x = FitParameter(name="mot_manta_sigma_x", fit=y_gauss_x, parameter="sigma")
    sigma_y = FitParameter(name="mot_manta_sigma_y", fit=y_gauss_y,parameter="sigma")
    bm_gauss_x = y_bm_gauss_x
    bm_gauss_y = y_bm_gauss_y
    bm_sigma_x = FitParameter(name="mot_manta_bm_sigma_x", fit=y_bm_gauss_x, parameter="g1_sigma")
    bm_sigma_y = FitParameter(name="mot_manta_bm_sigma_y", fit=y_bm_gauss_y, parameter="g1_sigma")
    bm_sigma2_x = FitParameter(name="mot_manta_bm_sigma2_x", fit=y_bm_gauss_x, parameter="g2_sigma")
    bm_sigma2_y = FitParameter(name="mot_manta_bm_sigma2_y", fit=y_bm_gauss_y, parameter="g2_sigma")
    atom_number = y_atom_number
    density = y_density
    imaging_delay = Param("sequence.imaging.delay", name="imaging_delay")
    cross_section = y_cross_section


manta = Y
mako = X
