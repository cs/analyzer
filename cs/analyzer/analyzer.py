from bidict import bidict
from dataclasses import dataclass
from functools import partial, reduce
import itertools
import networkx as nx
import operator
from typing import Any, Dict, List
import xarray as xr

from cs.analyzer import analysis, executors, loader, parallel


parallel.init_parallel()


@dataclass
class AnalyzerExec:
    name: str
    instance: analysis.AnalysisBase
    parameter_mapping: Dict[str, str]


class Analyzer:
    """class for collecting analysis scripts to be run on files

    The task of this class is to provide everything needed to get the desired
    analysis results from a file. For every analysis result it tracks the
    required steps needed to get this result. When intermediate results are
    needed by multiple later analysis steps they are computed only once.
    For every analysis the results are wrapped in a xarray structure.

    To track the analysis steps and there dependencies they are ordered in a
    graph structure. Every node is an analysis step, every edge corresponds to
    a dependency. The edges are directed, pointing to the required dependencies.
    To get all steps necessary to compute a single analysis step one can follow
    all outgoing edges. All nodes reachable from a node must be computed before
    the node itself can be computed.

    Attributes
    ----------
    _analyzers : nx.MultiDiGraph
        internal dependency graph of all analysis steps tracked by this Analyzer.
        Nodes correspond to analysis steps, the directed edges point towards the
        inputs which are required to compute the node.
    _output_nodes : bidict[output name, internal node name]
        dict to store all nodes which should be part of the output. The results
        from all other nodes are dropped as soon as the computation is finished.
    _ordered_analyzers : List[analysis.AnalysisBase]
        linearised version of the dependencies generated from `_analyzers`. It
        contains all required dependencies steps in an order which allows for
        linear processing. For all nodes it is guaranteed that it dependencies
        are at earlier positions in the list.
    _parallel: parallel.Parallel
        internal helper class for parallel execution of the analysis. This is
        used to parallelise work when multiple files should be analysed. The
        analysis of a single file is always sequential.
    _loader: loader.Loaader
        internal helper class for loading data from a file. By default data is
        loaded from a hdf5 file, but other loaders can be used, e.g. for testing
    _executor: executors.Executor
        internal helper class for performing the actual analysis.
    """

    def __init__(self, *analyzers, _loader=loader.HDF5Loader()):
        """construct a new Analyzer instance with the given analyzers

        Parameters
        ----------
        analyzers:
            Analyzers to add on construction of the `Analyzer`. Identically to
            calling `add_analyzers(analyzers)` on the constructed Analyzer.
        _loader: FileLoader
            Internal class used to load the data from a file. By default
            `HDF5Loader` is used to load data from an hdf5 file. Used for
            testing.
        """
        self._analyzers = nx.MultiDiGraph()
        self._output_nodes = bidict()
        self._ordered_analyzers: List[analysis.AnalysisBase] = []
        self.add_analyzer("time", analysis.RunTimestamp())
        self.add_analyzers(*analyzers)

        self._parallel = parallel.Parallel.get()
        self._loader = _loader
        self._executor = executors.Executor(self._loader)

    @property
    def analyzers(self) -> Dict[str, analysis.AnalysisBase]:
        """all analysis steps which should be computed by the analyzer

        this only contains the nodes which should be computed, not the required
        dependencies.

        Returns
        -------
        a dict from analyzer name to the analysis steps to perform
        """
        return {
            out_name: self._analyzers.nodes[int_name]["instance"]
            for out_name, int_name in self._output_nodes.items()
            if out_name not in set(("time", "it"))
        }

    def __add__(self, other: "Analyzer") -> "Analyzer":
        return Analyzer(*self.analyzers.items(), *other.analyzers.items())

    def __iadd__(self, other: "Analyzer") -> "Analyzer":
        self.add_analyzers(*other.analyzers.items())
        return self

    def add_function(self, name, function):
        """add a function, wrapping it in an analyzer

        :param name: str
            desired name of the analyzers results in the output xarray
        :param function: Callable
            function to wrap
        """
        self.add_analyzers((name, analysis.analysis(function)))

    def add_analyzer(self, name, analyzer=None):
        if analyzer is None:
            # one argument -> name == analyzer
            self.add_analyzers(name)
        else:
            self.add_analyzers((name, analyzer))

    def add_analyzers(self, *analyzers):
        for analyzer in analyzers:
            try:
                name, analyzer = analyzer
            except TypeError:
                name = analyzer.name

            if name in self._output_nodes:
                raise AttributeError(f"analyzer name {name} already in use")

            node_name = self._add_analyzer(analyzer)
            self._output_nodes.inverse[node_name] = name

        self._update_dependencies()
        self._ordered_analyzers = self._get_ordered_analyzer_list()

    def remove_analyzer(self, name):
        if name not in self._output_nodes:
            raise KeyError(f"no analyzer with name {name} defined for analyzer")

        self._remove_node_recursive(self._output_nodes[name])
        del self._output_nodes[name]
        self._update_dependencies()
        self._ordered_analyzers = self._get_ordered_analyzer_list()

    def _add_analyzer(self, analyzer: analysis.AnalysisBase):
        if not isinstance(analyzer, analysis.AnalysisBase):
            if callable(analyzer):
                raise AttributeError(
                    "analyzers must inherit from analyzer.analysis.AnalysisBase! "
                    f"{analyzer} is a function and should be wrapped first."
                    "Either use @analysis decorator or use "
                    "Analyzer.add_function"
                )
            else:
                raise AttributeError(
                    "analyzers must inherit from analyzer.analysis.AnalysisBase! "
                    f"{analyzer} does not"
                )
        return self._add_node_with_deps(analyzer)

    def _add_node_with_deps(self, node):
        if not isinstance(node, analysis.AnalysisBase):
            # we assume it is a constant value. We wrap it in a Constant
            # to fit it into the graph system
            node = analysis.Constant(node)

        if node.node_name in self._analyzers.nodes:
            # node already existent, no need to process it twice
            return node.node_name

        self._analyzers.add_node(node.node_name, instance=node)

        for parameter_name, dep in node.parameters.items():
            dep_name = self._add_node_with_deps(dep)
            self._analyzers.add_edge(
                node.node_name, dep_name, parameter_name=parameter_name
            )

        return node.node_name

    def _remove_node_recursive(self, node_name):
        if self._analyzers.in_degree(node_name) > 0:
            # node required as dependency for some other node. keep it
            return

        sub_nodes = list(self._analyzers.out_edges(node_name))
        self._analyzers.remove_node(node_name)
        for sub_node in sub_nodes:
            self._remove_node_recursive(sub_node[1])

    def _update_dependencies(self):
        """update the cache list of entries required from the hdf file
        """
        self._dependencies = reduce(
            operator.add,
            (
                analyzer[1]["instance"].get_dependencies()
                for analyzer in self._analyzers.nodes(data=True)
            ),
            analysis.Dependencies(),
        )

    def _get_ordered_analyzer_list(self):
        """order all analyzers in a structure for linear execution

        :return: List[Analysis]
            list with all analyzers script ordered for correct execution.
            When the list is processed from start to end the required dependencies
            will be computed for every analyzer in time.
        """
        ordered_analyzers = nx.topological_sort(self._analyzers)
        return list(
            reversed(
                [
                    AnalyzerExec(
                        name=a,
                        instance=self._analyzers.nodes[a]["instance"],
                        parameter_mapping=self._generate_parameter_mapping(a),
                    )
                    for a in ordered_analyzers
                ]
            )
        )

    def _generate_parameter_mapping(self, node):
        """generate a structure assigning a node name to each parameter name

        :return: Dict
            a dict mapping parameter names to node names
        """
        edges = self._analyzers.out_edges(node, data="parameter_name")
        return {
            parameter_name: dependency_name
            for _, dependency_name, parameter_name in edges
        }

    def analyze_file(self, filename, dimension_names="time"):
        return self._executor.run_multiple_on_file(
            filename=filename,
            analyzers=self._ordered_analyzers,
            data_paths=self._dependencies.hdf_entries,
            output_mapping=self._output_nodes.inverse,
            dimension_names=dimension_names,
        )

    def analyze_multiple(self, files, dimension_name="time"):
        return xr.concat(
            self._analyze_multiple(
                files,
                dimension_name,
                extra_data_paths=[],
            ),
            dim=dimension_name,
        )

    def _analyze_multiple(self, files, dimension_names, **kwargs):
        return self._parallel.map(
            partial(
                self._executor.run_multiple_on_file,
                analyzers=self._ordered_analyzers,
                data_paths=self._dependencies.hdf_entries,
                output_mapping=self._output_nodes.inverse,
                dimension_names=dimension_names,
                **kwargs,
            ),
            files,
        )

    def analyze_folder(self, day, folder, *, max_count=None):
        """run the analysis scripts on all files in the specified folder

        :param day: datetime.date or str
            day when the measurement was taken. When a string is passed
            it must be in isoformat, i.e. YYYY-MM-DD
        :param folder: str
            name of the subfolder below the day structure
        :param max_count: int (optional)
            defines an upper bound for the number of data sets to analyze. All 
            are analyzed if not specified
        :return: xr.Dataset
            a xarray Dataset containing the output of the different analysers.
            The main coordinate is the time when the measurement was taken
        """
        files = self._loader.list_folder(day, folder)

        if max_count is not None:
            files = itertools.islice(files, max_count)

        return xr.concat(
            self._analyze_multiple(files, dimension_names="time"), dim="time"
        )

    def analyze_iteration(
        self, day, iteration_name, *, max_count=None, dimension_names="iter"
    ):
        """run the analysis scripts on the given iteration

        :param day: datetime.date or str
            day when the measurement was taken. When a string is passed
            it must be in isoformat, i.e. YYYY-MM-DD
        :param iteration_name: str
            name of the iteration without the it_ prefix
        :param max_count: int (optional)
            defines an upper bound for the number of data sets to analyze. All 
            are analyzed if not specified
        :param dimension_names: List[str] (optional)
            by default different the different entries of the iter_value array are labeled
            "iter", "iter2", "iter3", etc. Instead names may be specified as a list of
            strings using this parameter.
        :return: xr.Dataset
            a xarray Dataset containing the output of the different analysers.
            The main coordinates are run and iter. run is an index counting multiple
            iterations in the same folder. iter contains the iter_value of each iteration.

            It contains an extra data variable "existing" indicating when a dataset was not
            taken. It can be false when runs use different iter_values.
        """
        files = self._loader.list_iteration_files(day, iteration_name)

        if max_count is not None:
            files = itertools.islice(files, max_count)

        analyzed = [
            self._analyze_multiple(
                map(str, files_per_run), dimension_names=dimension_names
            )
            for files_per_run in files
        ]
        
        run_data = []
        for run in analyzed:
            try:
                run_data.append(xr.concat([data for data in run if data is not None] , dim='iter'))
            except TypeError: #in case all shots of the run have failed
                pass
        
        
        if len(run_data) > 1:
            single_data = xr.concat(
                run_data,
                dim="run",
                fill_value=0,  # otherwise it will use nan and broadcast everything to float
            )
        else:
            single_data = run_data[0].expand_dims("run")

        return single_data

        series_data = self._executor.run_series_analyzers(
            self._ordered_analyzers, data=single_data
        )
        return series_data
