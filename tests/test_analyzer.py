from cs.analyzer import Analyzer, analysis, make_analysis
from cs.analyzer.error import AnalyzerError

import numpy as np
import pathlib
import pytest


class Constant3(analysis.Analysis):
    def analyze(self, a=analysis.Constant(3)):
        return a


class Constant5(analysis.Analysis):
    def analyze(self, a=analysis.Constant(5)):
        return a

"""
def test_empty_analyzer(analyzer, empty_file):
    data = analyzer.analyze_file(empty_file)
    print(data)
    assert data.it_enabled == False
    assert data.it_value == -1
    assert data.it_step == -1
"""

def test_adding_analysis(empty_file):
    a = Analyzer()
    a.add_analyzer("out", Constant3())
    res = a.analyze_file(empty_file)
    print(res)
    assert res.out == 3


def test_run_twice(analyzer, empty_file):
    analyzer.add_analyzer("out", Constant3())
    res1 = analyzer.analyze_file(empty_file)
    print(res1)
    res2 = analyzer.analyze_file(empty_file)
    print(res2)
    assert res1.out == res2.out
    assert res1 == res2


def test_one_element_iteration(analyzer, test_data):
    analyzer.add_analyzer(Constant3())
    it_data = test_data.create_iteration(values=[[dict(value=2)]])

    hdf5_files = list(pathlib.Path(analyzer._loader.base_path).glob("**/*.hdf5"))
    assert len(hdf5_files) == 2
    data = analyzer.analyze_iteration(*it_data)
    print(data)
    assert data.Constant3 == 3
    assert data.dims["iter"] == 1
    assert data.dims["run"] == 1


def ana1d():
    return np.linspace(0, 1, 10)


def test_too_many_output_dimensions(analyzer, empty_file):
    analyzer.add_analyzer(make_analysis(dims=())(ana1d))
    with pytest.raises(AnalyzerError):
        analyzer.analyze_file(empty_file)


def test_1d_output_dimensions(analyzer, empty_file):
    analyzer.add_analyzer(make_analysis(dims=("x"))(ana1d))
    analyzer.analyze_file(empty_file)


def test_analyzer_attribute(analyzer):
    attr = analyzer.analyzers
    assert attr == dict()

    analyzer.add_analyzer("const", Constant3())
    attr = analyzer.analyzers
    assert len(attr) == 1
    assert "const" in attr
    assert isinstance(attr["const"], Constant3)


def test_analyzer_iadd(analyzer, empty_file):
    ana_add = Analyzer(Constant5())
    analyzer.add_analyzer(Constant3())
    out = analyzer.analyze_file(empty_file)
    assert "Constant3" in out
    assert out.Constant3 == 3
    assert "Constant5" not in out

    analyzer += ana_add
    out = analyzer.analyze_file(empty_file)
    assert "Constant3" in out
    assert out.Constant3 == 3
    assert "Constant5" in out
    assert out.Constant5 == 5


def test_analyzer_add(empty_file):
    ana_left = Analyzer(Constant3())
    ana_right = Analyzer(Constant5())

    ana = ana_left + ana_right
    out = ana.analyze_file(empty_file)
    assert "Constant3" in out
    assert out.Constant3 == 3
    assert "Constant5" in out
    assert out.Constant5 == 5

    out = ana_left.analyze_file(empty_file)
    assert "Constant3" in out
    assert out.Constant3 == 3
    assert "Constant5" not in out

    out = ana_right.analyze_file(empty_file)
    assert "Constant3" not in out
    assert "Constant5" in out
    assert out.Constant5 == 5
