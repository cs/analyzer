#!/usr/bin/env python3

import datetime
import h5py
import os
import shutil
import tempfile

from qcontrol3.tools import json


def unique_time():
    unique_time.time = unique_time.time + datetime.timedelta(seconds=1)
    return unique_time.time


unique_time.time = datetime.datetime(1970, 1, 1)


def make_standard_data():
    return {
        "task/iter/enabled": False,
        "task/iter/step": -1,
        "task/iter/value": -1,
        "task/run_timestamp": unique_time().astimezone().isoformat().encode(),
        "task/parameters": "{}",
    }


def add_iteration_data(d, iter_value, iter_step):
    d["task/iter/enabled"] = True
    d["task/iter/step"] = iter_step
    d["task/iter/value"] = iter_value


class TestData:
    def __enter__(self):
        self.base_path = tempfile.mkdtemp()
        self.date = "1970-01-01"
        self.date_dir = os.path.join(self.base_path, self.date.replace("-", "/"))
        os.makedirs(self.date_dir)
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        shutil.rmtree(self.base_path)

    def _create_file(self, dir, values, parameters=None, prefix=None, expconfig=False):
        entries = make_standard_data()
        entries.update(values)
        if parameters is not None:
            del entries["task/parameters"]
            entries.update({f"task/parameters/{name}": value for name, value in parameters.items()})

        fd, file_path = tempfile.mkstemp(suffix=".hdf5", dir=dir, prefix=prefix)
        with h5py.File(file_path, "w") as f:
            for entry, value in entries.items():
                f[entry] = value
        if expconfig:
            self._create_expconfig(file_path, {"dummy": 0})

        return file_path

    def _create_expconfig(self, file_path, entries):
        file_path = file_path.replace(".hdf5", "_expconfig.hdf5")

        with h5py.File(file_path, "w") as f:
            for entry, value in entries.items():
                f[entry] = value

        return file_path

    def create_file(self, values, parameters={}, subdir=None):
        if subdir is None:
            dir = tempfile.mkdtemp(dir=self.date_dir)
        else:
            dir = os.path.join(self.date_dir, subdir)
            os.makedirs(dir, exist_ok=True)
        return self._create_file(dir, values, parameters)

    def create_iteration(self, values, parameters=None):
        dir = tempfile.mkdtemp(dir=self.date_dir, prefix="it_")
        subfolder = os.path.basename(dir)[3:]
        if parameters is None:
            parameters = [{}] * len(values)
        for run, (run_data, param) in enumerate(zip(values, parameters)):
            expconfig = True
            for i, data in enumerate(run_data):
                add_iteration_data(
                    data, iter_step=i, iter_value=data.get("iter_value", i)
                )
                self._create_file(
                    dir,
                    data,
                    prefix=f"{run:02}_{i:04}_",
                    expconfig=expconfig,
                    parameters=param,
                )
                expconfig = False

        return self.date, subfolder
