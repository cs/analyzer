from .dummy_data import TestData

import h5py
import os
import pathlib


def test_dummy_data(test_data):
    file_path = test_data.create_file(dict(value=3))
    assert os.path.exists(file_path)
    with h5py.File(file_path, "r") as f:
        assert f["value"][()] == 3


def test_dummy_iteration(test_data):
    values = [[dict(value=3)], [dict(value=4), dict(value=5)]]
    date, subfolder = test_data.create_iteration(values)
    p = pathlib.Path(test_data.date_dir) / f"it_{subfolder}"
    assert p.exists()
    print(list(p.iterdir()))
    assert len(list(p.iterdir())) == 5  # 3 + 2 expconfigs

    for glob, v in (("*0_*0_*.hdf5", 3), ("*1_*0_*.hdf5", 4)):
        file = list(p.glob(glob))
        print(file)

        for fi in file:
            if "expconfig" in str(fi):
                continue

            with h5py.File(fi, "r") as f:
                assert f["value"][()] == v
                assert f["task/iter/enabled"][()] == True
                assert f["task/iter/step"][()] == 0
                assert f["task/iter/value"][()] == 0


def test_time_order(dummy_analyzer, test_data):
    values = [[dict(value=i) for i in range(100)]]
    it_data = test_data.create_iteration(values)
    data = dummy_analyzer.analyze_iteration(*it_data)
    print(data)
