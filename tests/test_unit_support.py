import pytest
from cs.analyzer import Analysis
import cs.units as u


class UnitTester(Analysis):
    def analyze(self, value):
        return value


def recombine_unit(value, unit):
    return value << u.Unit(unit)


test_units = set((u.mm, u.m, u.mW / u.cm ** 2, u.J, 1 / u.s, u.Hz))


@pytest.mark.parametrize("input", test_units)
def test_single_return_unit(analyzer, empty_file, input):
    analyzer.add_analyzer(UnitTester(parameters=dict(value=1 * input)))
    res = analyzer.analyze_file(empty_file)
    print(res)
    assert res.UnitTester.dtype == float
    assert res.UnitTester == 1
    print(res.UnitTester)
    print(res.UnitTester.attrs)
    assert recombine_unit(res.UnitTester, res.UnitTester.attrs["unit"]) == input
