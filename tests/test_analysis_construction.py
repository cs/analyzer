import pytest
from cs.analyzer.analysis import Analysis, make_analysis

from tests.helper import DummyParam


@make_analysis()
def function(a=DummyParam(3), b=DummyParam(4)):
    return a, b


@make_analysis
def function_no_parantheses(a=DummyParam(3), b=DummyParam(4)):
    return a, b


@make_analysis(name="other")
def function_other_name(a=DummyParam(3), b=DummyParam(4)):
    return a, b


@make_analysis(dims=["x", "y"])
def function_other_dims(a=DummyParam(3), b=DummyParam(4)):
    return a, b


class AllParameters(Analysis):
    parameters = dict(a=DummyParam(3), b=DummyParam(4))

    def analyze(self, a, b):
        return a, b


class OneParameter(Analysis):
    parameters = dict(a=DummyParam(3))

    def analyze(self, a, b):
        return a, b


class NoParameters(Analysis):
    def analyze(self, a, b):
        return a, b


class OnlyAnalyze(Analysis):
    def analyze(self, a=DummyParam(3), b=DummyParam(4)):
        return a, b


def test_function_construction():
    print(type(function))
    assert isinstance(function, Analysis)
    assert isinstance(function_no_parantheses, Analysis)
    assert isinstance(function_other_name, Analysis)
    assert isinstance(function_other_dims, Analysis)


inputs = [
    (AllParameters(), (3, 4)),
    (AllParameters(parameters=dict(a=DummyParam(5))), (5, 4)),
    (NoParameters(parameters=dict(a=DummyParam(3), b=DummyParam(4))), (3, 4)),
    (OneParameter(parameters=dict(b=DummyParam(4))), (3, 4)),
    (OnlyAnalyze(), (3, 4)),
    (OnlyAnalyze(parameters=dict(a=DummyParam(5))), (5, 4)),
    (function, (3, 4)),
    (function_no_parantheses, (3, 4)),
    (function_other_name, (3, 4)),
    (function_other_dims, (3, 4)),
]


@pytest.mark.parametrize("a,expected", inputs)
def test_only_analyze_override(a, expected):
    assert set(a.parameters.keys()) == set(["a", "b"])
    for dep, exp in zip(a.parameters.values(), expected):
        assert isinstance(dep, DummyParam)
        assert dep.value == exp


def test_function_callable():
    ret = function(4, 5)
    assert ret == (4, 5)


def test_function_names():
    assert function_other_name.name == "other"
    assert function.name == "function"


def test_function_dims():
    assert function_other_dims.dims == ["x", "y"]
    assert function.dims == ()
