from dataclasses import dataclass
import numpy as np
from typing import Dict, List

from cs.analyzer import analysis


class DummyFile(dict):
    def __init__(self, iter_step=None, iter_value=None, **kwargs):
        super().__init__(**kwargs)
        if iter_value is None:
            self["task/iter/enabled"] = False
            self["task/iter/step"] = -1
            self["task/iter/value"] = -1
        else:
            self["task/iter/enabled"] = True
            self["task/iter/step"] = iter_step
            self["task/iter/value"] = iter_value


@dataclass
class DummyLoader:
    files: Dict
    iterations: List

    def load(self, file_path, data_paths):
        return {path: self.files[file_path][path] for path in data_paths}

    def list_folder(self, day, subfolder):
        return self.files.keys()

    def list_iteration_files(self, day, iteration_name):
        return self.iterations


DummyParam = analysis.Constant


dummy_file_data = {
    "task/iter/enabled": False,
    "task/iter/value": -1,
    "task/tstamp_run": np.asarray(b"20191107_164622_123456"),
}
