import numpy as np
import os
import pytest

from cs.analyzer.analysis import Constant
from cs.analyzer import Analyzer
from cs.analyzer.loader import HDF5Loader
from .dummy_data import TestData


@pytest.fixture
def test_data():
    with TestData() as d:
        assert os.path.exists(d.base_path)
        assert os.path.exists(d.date_dir)
        print(f"TestData(base_path={d.base_path})")
        yield d
    assert not os.path.exists(d.base_path)


@pytest.fixture
def empty_file(test_data):
    return test_data.create_file({}, {})


@pytest.fixture
def mot_image_file(test_data):
    data = {
        "data/mot/image/1/data": np.random.randint(0, 4096, size=(200, 200)),
        "data/mot/image/2/data": np.random.randint(0, 4096, size=(200, 200)),
        "data/mot/image/3/data": np.random.randint(0, 4096, size=(200, 200)),
    }
    from qcontrol3.tools.units import u

    params = dict(
        measured=dict(
            imaging=dict(
                mot=dict(pixel_size=24 * u.um, intensity=30 * u.mW / u.cm ** 2)
            )
        )
    )

    return test_data.create_file(data, parameters=params)


@pytest.fixture
def analyzer(test_data):
    return Analyzer(_loader=HDF5Loader(base_path=test_data.base_path))


@pytest.fixture
def dummy_analyzer(analyzer):
    analyzer.add_analyzer("out", Constant(0))
    return analyzer
