from cs.analyzer import make_analysis, Constant


@make_analysis
def plus(a=Constant(3), b=Constant(3)):
    return a + b


def test_dependency_added_twice(analyzer, empty_file):
    start_len = len(analyzer._ordered_analyzers)
    # add a node with two identical deps
    analyzer.add_analyzer(plus)
    assert len(analyzer._ordered_analyzers) == 2 + start_len
    ret = analyzer.analyze_file(empty_file)
    print(ret)
    assert ret.plus == 6
