import cs.analyzer.scripts.base.fit as fittools
import numpy as np
import lmfit

def test_gauss1d():
    x = np.linspace(-4,4,100)
    g1d_params = {'c':1, 'amplitude':2, 'sigma':1.5, 'center':2}
    m = fittools.make_gauss_1d_model()
    p = m.make_params(**g1d_params)
    g1d_values = m.eval(p, x=x)
    f = fittools.gauss_1d(g1d_values, x=x)

    assert np.isclose(f.params['amplitude'].value, p['amplitude'].value)
    assert np.isclose(f.params['sigma'].value, p['sigma'].value)
    assert np.isclose(f.params['center'].value, p['center'].value)
    assert np.isclose(f.params['c'].value, p['c'].value)

def test_bimodal():
    x = np.linspace(-4,4,100)
    bimodal_model = lmfit.models.GaussianModel(prefix='g1_') + lmfit.models.GaussianModel(
            prefix='g2_') + lmfit.models.ConstantModel()
    bimodal_model.set_param_hint('g1_amplitude', min=0)
    bimodal_model.set_param_hint('g2_amplitude', min=0)
    bimodal_model.set_param_hint('g1_sigma', min=0, max=4)
    bimodal_model.set_param_hint('g2_sigma', min=4)

    bm_params = {'c':1, 'g1_amplitude':2, 'g2_amplitude':30, 'g1_sigma':1, 'g2_sigma':6, 'g1_center':0, 'g2_center':0}
    m = bimodal_model
    p = m.make_params(**bm_params)
    bm_values = m.eval(p, x=x)
    f = fittools.bimodal_gauss_1d(bm_values, x=x, sigma_cut=4)

    assert np.isclose(f.params['g1_amplitude'].value, p['g1_amplitude'].value)
    assert np.isclose(f.params['g1_sigma'].value, p['g1_sigma'].value)
    assert np.isclose(f.params['g1_center'].value, p['g1_center'].value)
    assert np.isclose(f.params['c'].value, p['c'].value)

def test_polylog():
    x = np.linspace(-4,4,100)
    p52_params = {'x0':0, 'A':2, 'size':1, 'mu':0, 'c':3}
    m = fittools.polylog_52_model + lmfit.models.ConstantModel()
    p = m.make_params(**p52_params)
    p52_values = m.eval(p, x=x)
    f = fittools.fit_polylog_52(p52_values, x=x)

    assert np.isclose(f.params['x0'].value, p['x0'].value)
    assert np.isclose(f.params['A'].value, p['A'].value)
    assert np.isclose(f.params['size'].value, p['size'].value)
    assert np.isclose(f.params['c'].value, p['c'].value)

def test_gauss2d():
    x = np.linspace(-4,4,100)
    x,y = np.meshgrid(x,x)
    g2d_params = {'c':1, 'A':2, 'x0':.5, 'sx':2, 'y0':.3, 'sy':1.5}
    m = fittools.gauss_2d_model + lmfit.models.ConstantModel()
    p = m.make_params(**g2d_params)
    g2d_values = m.eval(p, x=x, y=y)
    f = fittools.gauss_2d(g2d_values, x=x, y=y)

    assert np.isclose(f.params['A'].value, p['A'].value)
    assert np.isclose(f.params['sx'].value, p['sx'].value)
    assert np.isclose(f.params['x0'].value, p['x0'].value)
    assert np.isclose(f.params['c'].value, p['c'].value)