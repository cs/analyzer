from cs.analyzer.analysis import HDF, Param


def test_hdf(analyzer, test_data):
    f = test_data.create_file(values=dict(number=3, string="tüst"))
    analyzer.add_analyzer("number", HDF("number"))
    analyzer.add_analyzer("string", HDF("string"))
    ret = analyzer.analyze_file(f)
    assert ret.number == 3
    assert ret.string == "tüst".encode('utf-8')


def test_param(analyzer, test_data):
    f = test_data.create_file(values={}, parameters=dict(test=3))
    analyzer.add_analyzer("test", Param("test"))
    ret = analyzer.analyze_file(f)
    assert ret.test == 3
